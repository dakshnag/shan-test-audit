import boto3
import botocore
from botocore.exceptions import ClientError
import argparse
import os
import json
import re
import sys
import base64


AWS_REGION = "us-west-2"
CUSTOMER_NUMBER = "00015021"

def parse_args():
    parser = argparse.ArgumentParser(description='deploy a ClearDATA VPC')

    #parser.add_argument('--InstancesStackName', default='instances', help='override name for the instances stack')
    #parser.add_argument('stackname', help='base stack name')
    #parser.add_argument('amifilepath', help='ami ids file')
    #parser.add_argument('paramspath', help='path to a paramaters file')
    parser.add_argument('awsregion', help='aws region')
    parser.add_argument('deployconfpath', help='path to deploy conf file')

    args = parser.parse_args()
    return args

def get_deploy_conf(path, separator='=', inLineReplace=None):
    if not os.path.exists(path):
        raise ValueError("cannot find {}".format(path))

    deploy_conf = dict((line.strip().split(separator, 1) for line in file(path) if separator in line))

    if inLineReplace:
        pattern = re.compile('<<\w+>>')
        for k,v in deploy_conf.iteritems():
            matches = pattern.findall(v)
            for m in matches:
                myKey = re.search('<<(.*)>>', m).group(1)
                if myKey not in deploy_conf: deploy_conf[myKey] = ''
                v = v.replace(m, deploy_conf[myKey])
            deploy_conf[k] = v

    return deploy_conf

def try_get_stack(session, name):
    try:
        client = session.client('cloudformation')
        response = client.describe_stacks(
            StackName=name
        )
        stacks = response.get('Stacks', [])
        if stacks:
            return stacks[0]
        else:
            return None
    except ClientError as ex:
        if ex.response['Error']['Code'] == 'ValidationError':
            return None
        else:
            raise (ex)


def flatten_params(params):
    return [dict(ParameterKey=k, ParameterValue=v) for k, v in params.iteritems()]


def create_or_update_stack_from_url(session, name, params, url, wait_until_avail=True):
    flatened_params = flatten_params(params)
    client = session.client('cloudformation')

    try:
        if try_get_stack(session, name):
            print("[+] Update stack '{}'".format(name))
            response = client.update_stack(
                StackName=name,
                TemplateURL=url,
                Parameters=flatened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_update_complete')
        else:
            print("[+] Creating new stack '{}'".format(name))
            response = client.create_stack(
                StackName=name,
                TemplateURL=url,
                Parameters=flatened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_create_complete')
        print("waiting for stack to be ready: '{}'".format(name))
        waiter.wait(StackName=name)
    except botocore.exceptions.ClientError as err:
        if re.search("No updates are to be performed", str(err)):
            print("[-] Nothing to update")
            return None
        else:
            raise err

    return response

def create_or_update_stack(session, name, params, template_path, wait_until_avail=True):
    flattened_params = flatten_params(params)
    client = session.client('cloudformation')
    with open(template_path) as f:
        body = f.read()

    try:
        if try_get_stack(session, name):
            print("[+] Update stack '{}'".format(name))
            response = client.update_stack(
                StackName=name,
                TemplateBody=body,
                Parameters=flattened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_update_complete')
        else:
            print("[+] Creating new stack '{}'".format(name))
            response = client.create_stack(
                StackName=name,
                TemplateBody=body,
                Parameters=flattened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_create_complete')
        print("waiting for stack to be ready: '{}'".format(name))
        waiter.wait(StackName=name)
    except botocore.exceptions.ClientError as err:
        if re.search("No updates are to be performed", str(err)):
            print("[-] Nothing to update")
            return None
        else:
            raise err

    return response

def create_rds_stack(session, name, params, rds_path, wait_until_avail=True):
    flatened_params = flatten_params(params)
    client = session.client('cloudformation')

    with open(rds_path) as f:
        body=f.read()

    response = client.create_stack(
        StackName=name,
        TemplateBody=body,
        Parameters=flatened_params,
        Capabilities=[
            'CAPABILITY_IAM'
        ]
    )

    if wait_until_avail:
        waiter = client.get_waiter('stack_create_complete')
        waiter.wait(StackName=name)

    return response


def extract_outputs(stack_description):
    extracted = [x for x in stack_description.get('Outputs', [])]
    ret = {}
    for x in extracted:
        ret[x['OutputKey']] = x['OutputValue']
    return ret

def extract_resources(session, stack_name):
    client = session.client('cloudformation')
    response = client.describe_stack_resources(StackName=stack_name)

    ret = {}
    for x in response.get('StackResources', []):
        ret[x['LogicalResourceId']] = x['PhysicalResourceId']
    return ret

def extract_single_resource(session, stack_name, logical_resource_id):
    client = session.client('cloudformation')
    response = client.describe_stack_resource(StackName=stack_name, LogicalResourceId=logical_resource_id)

    return response.get('StackResourceDetail' ,[])['PhysicalResourceId']

def deploy_vpc(session, deploy_conf):
    print("ensuring VPC")
    vpc_stack_name = '{}{}-{}'.format(deploy_conf['CMN_Profile'],deploy_conf['CMN_Environment'],deploy_conf['CMN_Application'])
    vpc_params = {
        "PrivateSubnet0CIDR": deploy_conf['VPC_PrivateSubnet0CIDR'],
        "PrivateSubnet1CIDR": deploy_conf['VPC_PrivateSubnet1CIDR'],
        "PrivateSubnet2CIDR": deploy_conf['VPC_PrivateSubnet2CIDR'],
        "PublicSubnet0CIDR": deploy_conf['VPC_PublicSubnet0CIDR'],
        "PublicSubnet1CIDR": deploy_conf['VPC_PublicSubnet1CIDR'],
        "PublicSubnet2CIDR": deploy_conf['VPC_PublicSubnet2CIDR'],
        "Environment": deploy_conf['CMN_Environment'],
        "Profile": deploy_conf['CMN_Profile'],
        "Application": deploy_conf['CMN_Application'],
        "VPCCIDR": deploy_conf['VPC_VPCCIDR']
    }
    created = create_or_update_stack(session, vpc_stack_name, vpc_params, "cloudformation/vpc.yml", wait_until_avail=True)
    existing_vpc = try_get_stack(session, vpc_stack_name)
    vpc_outputs = extract_outputs(existing_vpc)

    # # Create SG
    # print("ensuring SG")
    # sg_stack_name = '{}-audit-sg'.format(stack_name)
    # sg_params = {
    #     "DeployGroup": deploy_conf['SG_DeployGroup'],
    #     "IngressCidrIpMyIp1": deploy_conf['SG_IngressCidrIpMyIp1'],
    #     "IngressCidrIpMyIp2": deploy_conf['SG_IngressCidrIpMyIp2'],
    #     "IngressCidrIpVPC": deploy_conf['SG_IngressCidrIpVPC'],
    #     "ProvisioningHostCIDR": deploy_conf['SG_ProvisioningHostCIDR'],
    #     "RocheStack": stack_name,
    #     "VpcId": vpc_outputs['VPC']
    # }
    # created = create_or_update_stack(session, sg_stack_name, sg_params, "cloudformation/sg-auditsvc.cfn.yml", wait_until_avail=True)
    # existing_sg = try_get_stack(session, sg_stack_name)
    # sg_outputs = extract_outputs(existing_sg)
    #
    # print("ensuring RDS ...")
    # rds_stack_name = '{}-audit-rds'.format(stack_name)
    # existing_rds = try_get_stack(session, rds_stack_name)
    # if existing_rds:
    #     print("{0} exists".format(rds_stack_name))
    # else:
    #     print("creating {0}".format(rds_stack_name))
    #     rds_params = {
    #         "DBBackupRetention": deploy_conf['RDS_DBBackupRetention'],
    #         "DBClass":  deploy_conf['RDS_DBClass'],
    #         "DBInstanceId": deploy_conf['RDS_DBInstanceId'],
    #         "DBPassword": decryptPassword(session,deploy_conf['RDS_DBPassword']),
    #         "DBPort":   deploy_conf['RDS_DBPort'],
    #         "DBSecurityGroups": sg_outputs['AuditDB'],
    #         "DBSubnets": vpc_outputs['PrivateSubnets'],
    #         "DBTimeZone": deploy_conf['RDS_TimeZone'],
    #         "DBUsername": deploy_conf['RDS_DBUsername'],
    #         "rochestackName": stack_name
    #     }
    #     created = create_rds_stack(session, rds_stack_name, rds_params, "cloudformation/aurora-postgres.cft.yml", wait_until_avail=True)
    #     existing_rds = try_get_stack(session, rds_stack_name)
    #     pass
    # rds_outputs = extract_outputs(existing_rds)
    #
    # # Create Audit Service SQS
    # print("ensuring Audit SQS")
    # sqs_audit_stack_name = '{}-audit-sqs'.format(stack_name)
    # sqs_params = {
    #     "ClientAccounts": deploy_conf['SQS_AUDIT_ClientAccounts'],
    #     "DeployGroup": deploy_conf['SQS_AUDIT_DeployGroup'],
    #     "RocheStack": stack_name,
    #     "ServiceName": deploy_conf['SQS_AUDIT_ServiceName'],
    #     "Environment": deploy_conf['application_profile_name']
    # }
    # created = create_or_update_stack(session, sqs_audit_stack_name, sqs_params, "cloudformation/sqs-auditsvc.cfn.yml", wait_until_avail=True)
    # existing_sqs = try_get_stack(session, sqs_audit_stack_name)
    # sqs_outputs = extract_outputs(existing_sqs)
    #
    # # Create Audit Service SQS Alarm
    # if deploy_conf['SQS_AUDIT_VictorAlarmRequired'] == "yes":
    #     print("ensuring Audit SQS Alarm")
    #     sqs_alm_audit_stack_name = '{}-sqs-alarms-audit'.format(stack_name)
    #     sqs_alm_params = {
    #         "AuditQueueName": sqs_outputs['AuditSvcQueueName'],
    #         "rochestack": stack_name
    #     }
    #     created = create_or_update_stack(session, sqs_alm_audit_stack_name, sqs_alm_params, "cloudformation/audit-alarms.cfn.yml", wait_until_avail=True)
    #     existing_sqs = try_get_stack(session, sqs_alm_audit_stack_name)
    #     sqs_outputs = extract_outputs(existing_sqs)
    #
    # # Create Bastion
    # print("ensuring Bastion")
    # bastion_stack_name = '{}-audit-bastion'.format(stack_name)
    # bastion_params = {
    #     "AZ1PUBLICSUBNET": vpc_outputs['PublicSubnets'].split(",")[0],
    #     "BastionAmi": deploy_conf['BASTION_BastionAmi'],
    #     "InstanceStackName": deploy_conf['BASTION_InstanceStackName'],
    #     "RocheCostCenter": deploy_conf['BASTION_RocheCostCenter'],
    #     "StandardMicroserviceInternalSecurityGroup": sg_outputs['AuditInternalSg'],
    #     "RocheStack": stack_name,
    #     "VPCCIDR": deploy_conf['BASTION_VPCCIDR'],
    #     "VPCID": vpc_outputs['VPC'],
    #     "INSTANCEKEY": deploy_conf['EC2_INSTANCEKEY']
    # }
    # created = create_or_update_stack(session, bastion_stack_name, bastion_params, "cloudformation/linux_bastion-auditsvc.cfn.yml", wait_until_avail=True)
    #
    # #Adding Parameter to Paramstore
    # print("ensuring Paramstore")
    # client = session.client('ssm')
    # parameterpath= "/platform/" + deploy_conf['CMN_stack_name'] + "/" + deploy_conf['CMN_deployment_group'] + "/audit/spring.datasource.password"
    # client.put_parameter(Name=parameterpath, Value=decryptPassword(session,deploy_conf['RDS_DBPassword']), Type='SecureString')



def get_params_file(path, stackname):
    if not os.path.exists(path):
        raise ValueError("cannot find {}".format(path))

    with open(path) as f:
        data = json.load(f)

    # cleanup, add in calculated values
    if "VPCName" not in data:
        data["VPCName"] = stackname

    return data

def get_ami_file(path):
    if not os.path.exists(path):
        raise ValueError("cannot find {}".format(path))

    with open(path) as f:
        data = json.load(f)

    return data

def decryptPassword(session,ciphertext):
    client = session.client('kms')
    plaintext = client.decrypt(
                CiphertextBlob=bytes(base64.b64decode(ciphertext))
            )
    return plaintext["Plaintext"].decode("utf-8")

def validate_params(params):
    # TODO: actually validate values
    pass


if __name__ == "__main__":
    args = parse_args()

    session = boto3.Session()

    #params = get_params_file(args.paramspath, args.stackname)
    #amiids = get_ami_file(args.amifilepath)
    AWS_REGION = args.awsregion
    #validate_params(params)
    deploy_conf = get_deploy_conf(args.deployconfpath, inLineReplace=1)

    deploy_vpc(session, deploy_conf)
