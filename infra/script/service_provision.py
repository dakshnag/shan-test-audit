import boto3
import botocore
from botocore.exceptions import ClientError
import argparse
import os
import json
import re
import sys
import base64


AWS_REGION = "us-west-2"
CUSTOMER_NUMBER = "00015021"

def parse_args():
    parser = argparse.ArgumentParser(description='deploy a ClearDATA VPC')

    parser.add_argument('--InstancesStackName', default='instances', help='override name for the instances stack')
    parser.add_argument('stackname', help='base stack name')
    #parser.add_argument('amifilepath', help='ami ids file')
    #parser.add_argument('paramspath', help='path to a paramaters file')
    parser.add_argument('awsregion', help='aws region')
    parser.add_argument('deployconfpath', help='path to deploy conf file')

    args = parser.parse_args()
    return args

def get_deploy_conf(path, separator='=', inLineReplace=None):
    if not os.path.exists(path):
        raise ValueError("cannot find {}".format(path))

    deploy_conf = dict((line.strip().split(separator, 1) for line in file(path) if separator in line))

    if inLineReplace:
        pattern = re.compile('<<\w+>>')
        for k,v in deploy_conf.iteritems():
            matches = pattern.findall(v)
            for m in matches:
                myKey = re.search('<<(.*)>>', m).group(1)
                if myKey not in deploy_conf: deploy_conf[myKey] = ''
                v = v.replace(m, deploy_conf[myKey])
            deploy_conf[k] = v

    return deploy_conf

def try_get_stack(session, name):
    try:
        client = session.client('cloudformation')
        response = client.describe_stacks(
            StackName=name
        )
        stacks = response.get('Stacks', [])
        if stacks:
            return stacks[0]
        else:
            return None
    except ClientError as ex:
        if ex.response['Error']['Code'] == 'ValidationError':
            return None
        else:
            raise (ex)


def flatten_params(params):
    return [dict(ParameterKey=k, ParameterValue=v) for k, v in params.iteritems()]


def create_or_update_stack_from_url(session, name, params, url, wait_until_avail=True):
    flatened_params = flatten_params(params)
    client = session.client('cloudformation')

    try:
        if try_get_stack(session, name):
            print("[+] Update stack '{}'".format(name))
            response = client.update_stack(
                StackName=name,
                TemplateURL=url,
                Parameters=flatened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_update_complete')
        else:
            print("[+] Creating new stack '{}'".format(name))
            response = client.create_stack(
                StackName=name,
                TemplateURL=url,
                Parameters=flatened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_create_complete')
        print("waiting for stack to be ready: '{}'".format(name))
        waiter.wait(StackName=name)
    except botocore.exceptions.ClientError as err:
        if re.search("No updates are to be performed", str(err)):
            print("[-] Nothing to update")
            return None
        else:
            raise err

    return response

def create_or_update_stack(session, name, params, template_path, wait_until_avail=True):
    flattened_params = flatten_params(params)
    client = session.client('cloudformation')
    with open(template_path) as f:
        body = f.read()

    try:
        if try_get_stack(session, name):
            print("[+] Update stack '{}'".format(name))
            response = client.update_stack(
                StackName=name,
                TemplateBody=body,
                Parameters=flattened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_update_complete')
        else:
            print("[+] Creating new stack '{}'".format(name))
            response = client.create_stack(
                StackName=name,
                TemplateBody=body,
                Parameters=flattened_params,
                Capabilities=[
                    'CAPABILITY_IAM'
                ]
            )
            waiter = client.get_waiter('stack_create_complete')
        print("waiting for stack to be ready: '{}'".format(name))
        waiter.wait(StackName=name)
    except botocore.exceptions.ClientError as err:
        if re.search("No updates are to be performed", str(err)):
            print("[-] Nothing to update")
            return None
        else:
            raise err

    return response

def create_rds_stack(session, name, params, rds_path, wait_until_avail=True):
    flatened_params = flatten_params(params)
    client = session.client('cloudformation')

    with open(rds_path) as f:
        body=f.read()

    response = client.create_stack(
        StackName=name,
        TemplateBody=body,
        Parameters=flatened_params,
        Capabilities=[
            'CAPABILITY_IAM'
        ]
    )

    if wait_until_avail:
        waiter = client.get_waiter('stack_create_complete')
        waiter.wait(StackName=name)

    return response


def extract_outputs(stack_description):
    extracted = [x for x in stack_description.get('Outputs', [])]
    ret = {}
    for x in extracted:
        ret[x['OutputKey']] = x['OutputValue']
    return ret

def extract_resources(session, stack_name):
    client = session.client('cloudformation')
    response = client.describe_stack_resources(StackName=stack_name)

    ret = {}
    for x in response.get('StackResources', []):
        ret[x['LogicalResourceId']] = x['PhysicalResourceId']
    return ret

def extract_single_resource(session, stack_name, logical_resource_id):
    client = session.client('cloudformation')
    response = client.describe_stack_resource(StackName=stack_name, LogicalResourceId=logical_resource_id)

    return response.get('StackResourceDetail' ,[])['PhysicalResourceId']

def deploy_service(session, stack_name, deploy_conf, instances_name_override=None):

    # #Adding Parameter to Paramstore
    # print("ensuring Paramstore")
    # client = session.client('ssm')
    # parameterpath= "/platform/" + deploy_conf['CMN_stack_name'] + "/" + deploy_conf['CMN_deployment_group'] + "/audit/spring.datasource.password"
    # client.put_parameter(Name=parameterpath, Value=decryptPassword(session,deploy_conf['RDS_DBPassword']), Type='SecureString')
    #GETTING BASE INFRA values
    vpc_stack_name = '{}-audit'.format(stack_name)
    existing_vpc = try_get_stack(session, vpc_stack_name)
    vpc_outputs = extract_outputs(existing_vpc)

    #Create ECS Cluster
    print("ensuring Cluster")
    cluster_stack_name = '{}-audit-cluster'.format(stack_name)
    cluster_params = {
        "DesiredCapacity": deploy_conf['CLUSTER_DesiredCapacity'],
        "ECSAMI": deploy_conf['CLUSTER_ECSAMI'],
        "InstanceType": deploy_conf['CLUSTER_InstanceType'],
        "KeyPairName": deploy_conf['EC2_INSTANCEKEY'],
        "MaxSize": deploy_conf['CLUSTER_MaxSize'],
        "VPCID": vpc_outputs['VPC'],
        "RocheStack": stack_name,
        "ecsPrivateSubnets": vpc_outputs['PrivateSubnets'],
        "ecsPublicSubnets": vpc_outputs['PublicSubnets']
    }
    created = create_or_update_stack(session, bastion_stack_name, bastion_params, "cloudformation/audit-ecs-asg-elb.cfn.yml", wait_until_avail=True)
    existing_cluster = try_get_stack(session, cluster_stack_name)
    cluster_outputs = extract_outputs(existing_cluster)



def get_params_file(path, stackname):
    if not os.path.exists(path):
        raise ValueError("cannot find {}".format(path))

    with open(path) as f:
        data = json.load(f)

    # cleanup, add in calculated values
    if "VPCName" not in data:
        data["VPCName"] = stackname

    return data

def get_ami_file(path):
    if not os.path.exists(path):
        raise ValueError("cannot find {}".format(path))

    with open(path) as f:
        data = json.load(f)

    return data

def decryptPassword(session,ciphertext):
    client = session.client('kms')
    plaintext = client.decrypt(
                CiphertextBlob=bytes(base64.b64decode(ciphertext))
            )
    return plaintext["Plaintext"].decode("utf-8")

def validate_params(params):
    # TODO: actually validate values
    pass


if __name__ == "__main__":
    args = ()

    session = boto3.Session()

    #params = get_params_file(args.paramspath, args.stackname)
    #amiids = get_ami_file(args.amifilepath)
    AWS_REGION = args.awsregion
    #validate_params(params)
    deploy_conf = get_deploy_conf(args.deployconfpath, inLineReplace=1)

    deploy_service(session, args.stackname, deploy_conf, instances_name_override=args.InstancesStackName)
