AWSTemplateFormatVersion: 2010-09-09
Description: Deploy a service into an ECS cluster behind a public load balancer.
Parameters:
  RocheStackName:
    Type: String
    Default: np160intdev-ecs-cluster
    Description: >-
      The name of the parent cluster stack that you created. CloudFormation
      template. Necessary to locate and reference resources created by that
      stack.
  ServiceName:
    Type: String
    Default: audit-service
    Description: A name for the service
  ImageUrl:
    Type: String
    Default: '665097691123.dkr.ecr.us-west-2.amazonaws.com/audit-service:latest'
    Description: >-
      The url of a docker image that contains the application process that will
      handle the traffic for this service.  change accountid.
  ImageURLssl:
    Type: String
    Default: '665097691123.dkr.ecr.us-west-2.amazonaws.com/https-sidecar:0.0.1'
    Description: url for https-nginx
  HostedZoneId:
    Type: String
    Default: ''
    Description: Hosted Zone ID.
  RecordName:
    Type: String
    Default: ''
    Description: Full Route53 dns with respect to the hosted zone.
  ContainerPort:
    Type: Number
    Default: 8080
    Description: What port number the application inside the docker container is binding to
  ContainerCpu:
    Type: Number
    Default: 1024
    Description: How much CPU to give the container. 1024 is 1 CPU
  ContainerMemory:
    Type: Number
    Default: 1024
    Description: How much memory in megabytes to give the container
  DesiredCount:
    Type: Number
    Default: 2
    Description: How many copies of the service task to run
  ParamStorePrefix:
    Type: String
    Default: /platform/np160intdev/Astack/audit/
    Description: Parameter
  SpringProfilesActive:
    Type: String
    Default: ''
    Description: ''
  ApplicationProfile:
    Type: String
    Default: dev
    Description: ''
  AuditCertificate:
    Type: String
    Default: >-
      arn:aws:acm:us-west-2:665097691123:certificate/c9996408-f25e-427e-9ae6-e01e4d319255
    Description: Audit URL - ACM ARN of the certificate
  ecsPublicSubnets:
    Type: 'List<AWS::EC2::Subnet::Id>'
    Default: ''
    Description: three public subnets seperated by comma
  ContainerCpuFamily:
    Type: String
    Default: '2048'
    Description: ''
  ContainerMemoryFamily:
    Type: String
    Default: '2048'
    Description: ''
Resources:
  TaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      Family: !Ref ServiceName
      Cpu: !Ref ContainerCpuFamily
      Memory: !Ref ContainerMemoryFamily
      TaskRoleArn: !ImportValue
        'Fn::Join':
          - ':'
          - - !Sub '${RocheStackName}-role'
            - Role
      ContainerDefinitions:
        - Name: !Ref ServiceName
          Cpu: !Ref ContainerCpu
          Memory: !Ref ContainerMemory
          Image: !Ref ImageUrl
          Essential: true
          Environment:
            - Name: PARAMETERS_PATH
              Value: !Ref ParamStorePrefix
            - Name: SPRING_PROFILES_ACTIVE
              Value: !Ref SpringProfilesActive
            - Name: APPLICATION_PROFILE
              Value: !Ref ApplicationProfile
            - Name: APPDYNAMICS_AGENT_ACCOUNT_NAME
              Value: roche-test
            - Name: APPDYNAMICS_AGENT_ACCOUNT_ACCESS_KEY
              Value: 8b15kf544uiq
            - Name: APPDYNAMICS_CONTROLLER_HOST_NAME
              Value: roche-test.saas.appdynamics.com
            - Name: APPDYNAMICS_CONTROLLER_PORT
              Value: 443
            - Name: APPDYNAMICS_CONTROLLER_SSL_ENABLED
              Value: true
            - Name: APPDYNAMICS_AGENT_APPLICATION_NAME
              Value: !Ref RocheStackName
            - Name: APPDYNAMICS_AGENT_TIER_NAME
              Value: !Sub '${RocheStackName}-audit'
          MountPoints:
            - ContainerPath: /var/logs/
              SourceVolume: logs
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref 'AWS::StackName'
              awslogs-region: !Ref 'AWS::Region'
              awslogs-stream-prefix: !Ref ServiceName
        - Name: https-sidecar
          Essential: true
          Image: !Ref ImageURLssl
          Cpu: 128
          Environment:
            - Name: SERVER_HOSTNAME
              Value: !Ref RecordName
            - Name: PROXY_TO
              Value: !Sub 'http://${ServiceName}:8080/'
            - Name: SELF_SIGNED
              Value: 1
          Links:
            - !Sub '${ServiceName}'
          MemoryReservation: 256
          PortMappings:
            - ContainerPort: 443
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref 'AWS::StackName'
              awslogs-region: !Ref 'AWS::Region'
              awslogs-stream-prefix: !Sub '${ServiceName}-https'
      Volumes:
        - Name: logs
          DockerVolumeConfiguration:
            Scope: task
  Service:
    Type: 'AWS::ECS::Service'
    DependsOn: PublicLoadBalancerListener
    Properties:
      ServiceName: !Ref ServiceName
      Cluster: !ImportValue
        'Fn::Join':
          - ':'
          - - !Sub '${RocheStackName}-ecs-cluster'
            - ClusterName
      DeploymentConfiguration:
        MaximumPercent: 200
        MinimumHealthyPercent: 75
      DesiredCount: !Ref DesiredCount
      TaskDefinition: !Ref TaskDefinition
      LoadBalancers:
        - ContainerName: https-sidecar
          ContainerPort: 443
          TargetGroupArn: !Ref TargetGroup
  ScalableTarget:
    Type: 'AWS::ApplicationAutoScaling::ScalableTarget'
    Properties:
      MaxCapacity: 10
      MinCapacity: 2
      ResourceId: !Join
        - /
        - - service
          - !ImportValue
            'Fn::Sub': '${RocheStackName}-ecs-cluster:ClusterName'
          - !GetAtt
            - Service
            - Name
      RoleARN: !ImportValue
        'Fn::Join':
          - ':'
          - - !Sub '${RocheStackName}-ecs-cluster'
            - ECSRole
      ScalableDimension: 'ecs:service:DesiredCount'
      ServiceNamespace: ecs
  ScalablePolicy:
    Type: 'AWS::ApplicationAutoScaling::ScalingPolicy'
    Properties:
      PolicyName: Audit-Service-ScalingPolicy
      PolicyType: TargetTrackingScaling
      ScalingTargetId: !Ref ScalableTarget
      TargetTrackingScalingPolicyConfiguration:
        TargetValue: 70
        ScaleInCooldown: 300
        ScaleOutCooldown: 300
        PredefinedMetricSpecification:
          PredefinedMetricType: ECSServiceAverageCPUUtilization
  CloudWatchLogsGroup:
    Type: 'AWS::Logs::LogGroup'
    Properties:
      LogGroupName: !Ref 'AWS::StackName'
      RetentionInDays: 365
  PublicLoadBalancer:
    Type: 'AWS::ElasticLoadBalancingV2::LoadBalancer'
    Properties:
      Scheme: internet-facing
      LoadBalancerAttributes:
        - Key: idle_timeout.timeout_seconds
          Value: '30'
      Subnets: !Ref ecsPublicSubnets
      SecurityGroups:
        - !ImportValue
          'Fn::Sub': '${RocheStackName}-sg:AuditElbSg'
  TargetGroup:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      HealthCheckIntervalSeconds: 10
      Matcher:
        HttpCode: '200'
      HealthCheckPath: /health
      HealthCheckProtocol: HTTPS
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2
      Name: !Join
        - '-'
        - - !Ref 'AWS::StackName'
          - audit
      Port: 443
      Protocol: HTTPS
      UnhealthyThresholdCount: 10
      VpcId: !ImportValue
        'Fn::Join':
          - ':'
          - - !Sub '${RocheStackName}-ecs-cluster'
            - VPCId
  PublicLoadBalancerListener:
    Type: 'AWS::ElasticLoadBalancingV2::Listener'
    Properties:
      Certificates:
        - CertificateArn: !Ref AuditCertificate
      DefaultActions:
        - TargetGroupArn: !Ref TargetGroup
          Type: forward
      LoadBalancerArn: !Ref PublicLoadBalancer
      Port: 443
      Protocol: HTTPS
  Route53Record:
    Type: 'AWS::Route53::RecordSet'
    Properties:
      HostedZoneId: !Ref HostedZoneId
      Name: !Ref RecordName
      ResourceRecords:
        - !GetAtt
          - PublicLoadBalancer
          - DNSName
      TTL: '60'
      Type: CNAME
Outputs:
  ExternalUrl:
    Description: The url of the external load balancer
    Value: !Join
      - ''
      - - 'https://'
        - !Ref Route53Record
        - /health
    Export:
      Name: !Join
        - ':'
        - - !Ref 'AWS::StackName'
          - ExternalUrl
